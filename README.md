# A framework for regression testing

## Tools
* [atest](atest) - see [atest.example.sh](atest.example.sh)
* [ucmp](ucmp) - `cmp` for numbers
* [udiff](udiff) - `diff` for numbers
* [uscale](uscale) - scale all numbers

## Installation

	make install PREFIX=<prefix directory>
	
scripts are installed to `$PREFIX/bin` and man pages are to
`$PREFIX/man`.
